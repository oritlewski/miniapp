let data = {};
let questions = {};
let title = '';
let progress = ''
let answer = '';
let countQuestions = 0;


window.addEventListener("load", function(event) {
     title = document.getElementById('question--title');
     progress = this.document.getElementById('progress');

    /**
     * Fetch json data and parse them
     * 
     * @returns
     */
    async function fetchDataJSON() {
        const response = await fetch("data.json");
        const data = await response.json();
        return data;
    }

    /**
     * Save json data to local Storage for futur usage
     * to avoid calling the API
     */
    fetchDataJSON().then(data => {
        localStorage.setItem('data', JSON.stringify(data));
    });

    data = JSON.parse(localStorage.getItem('data'));
    console.log(data);
    questions = data.questions;
    countQuestions = data.questions.length;

    // Define the title
    title.innerHTML = questions[0].question;

    // Set the progress text
    progress.innerHTML = 'Cette revision contient ' + countQuestions + ' questions.';

    // 
    answer = questions[0].answer.title;
});

/**
 * Answer verification
 * 
 * For the moment we just add a class to the element clicked
 * In the futur we can return a value true/false to manage the behavior in consequence
 */
function verification(objet) {
    image = document.getElementById('image-correction');
    image.classList.add('show-image');

    if (objet.id === questions[0].answer.id) {
        objet.classList.add('true');
    } else {
        objet.classList.add('false');
    }
}